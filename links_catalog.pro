# Drop unneeded.
QT -= qt core gui


CONFIG += link_pkgconfig
PKGCONFIG += gtk+-3.0
PKGCONFIG += sqlite3

INCLUDEPATH += include/
SOURCES = $$files(src/*.c)
HEADERS = $$files(include/*.h)
