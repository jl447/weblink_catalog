#ifndef LINKS_APP_H_
#define LINKS_APP_H_


#include "config.main.h"
#include <gtk/gtk.h>


/**
 * @brief do_app Runs main application;
 * @param app Gtk application object;
 * @param inf Links application info structure.
 */
void
do_app(GtkApplication *app, struct lc_application_info *inf);

#endif
