
#ifndef CONFIG_MAIN_H_
#define CONFIG_MAIN_H_

#include <gtk/gtk.h>

#define LINKS_CATALOG_APPLICATION_ID  "org.alpha.links_catalog"

#define APPLICATION_DIRECTORY         ".local/links_catalog"

#define INIT_SCRIPT_NAME              "initdb.sql"

#define LINKS_DB_NAME                 "links.db"


struct lc_application_info {
  GtkEntry *user_link_entry;
  GtkEntry *user_remark_entry;
  GtkLabel *status_label;
  GtkListBox *links_list_box;
  GtkEntry *filter_entry;
  void *data;
};


struct lc_application_config {
  char *homedir_path;
  char application_dir_path[NAME_MAX];
  char db_location[NAME_MAX];
  char init_script_location[NAME_MAX];
};

#endif
