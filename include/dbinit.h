/**
  Db initialisation routines for link catalog program.
  Its assumed they will be used in main().
*/
#ifndef DBINIT_H_
#define DBINIT_H_


#include <sqlite3.h>


#define NULCHR '\0'


static const char init_sql_string_default[] =
    "BEGIN TRANSACTION;\n"
    "\n"
    "CREATE TABLE links (\n"
    "    id      INTEGER PRIMARY KEY,\n"
    "    link    TEXT UNIQUE NOT NULL,\n"
    "    remark  TEXT NOT NULL\n"
    ");\n"
    "\n"
    "END TRANSACTION;\n";


/* Read sql expressions from default initialization script to c-str. */
/* Returns NULL on error.                                            */
char *
get_init_sql(const char *sql_fn);


/* Run default initialization script. Creates db with rw access.      */
/* Returns NULL on error.                                             */
sqlite3 *
init_db(const char *db_fn, const char *init_sql);


#endif
