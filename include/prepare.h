#ifndef PREPARE_H_
#define PREPARE_H_


#include "config.main.h"


/**
 * @brief prepare_application_directory Checks and configures all needed files
 * @param app_config Application parameters to init;
 * @return 0 if error, 1 otherwise.
 */
int
prepare_application_directory(struct lc_application_config *app_config);


#endif
