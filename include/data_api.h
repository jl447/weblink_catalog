#ifndef DATA_API_H
#define DATA_API_H

enum data_result {
  DATA_SUCCESS_FLAG,
  DATA_ERROR_FLAG,
  DATA_NO_MORE_DATA_FLAG
};

enum data_result
data_append_row(void *data, const char *link, const char *remark);

void
data_close(void *data);

enum data_result
data_next_record(void *data, char **link_column, char **remark_column);

enum data_result
data_open(void **data);

const char *
data_error_string(void *data_handle);

#endif /* DATA_API_H */
