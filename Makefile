CC = gcc
CFLAGS = -Wall -g -O0 -I./include/ -pipe
GTK3_CFLAGS = `pkg-config --cflags gtk+-3.0`
GTK3_LIBS = `pkg-config --libs gtk+-3.0`
SQLITE3_LIBS = `pkg-config --libs sqlite3`
OBJS = main.o dbinit.o prepare.o links_app.o data_sqlite3_api.o
OUTPUT = links_catalog


all: $(OUTPUT)


$(OUTPUT): $(OBJS)
	$(CC) $(SQLITE3_LIBS) $(GTK3_LIBS) -o $(OUTPUT) $^


%.o: src/%.c
	$(CC) $(CFLAGS) $(GTK3_CFLAGS) -c -o $@ $<


clean:
	rm -f $(OUTPUT) *.o
