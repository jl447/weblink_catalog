
#include <limits.h> /* For NAME_MAX. */
#include <string.h>
#include <sys/stat.h>

#include "prepare.h"


int
prepare_application_directory(struct lc_application_config *app_config) {
  char localdir[NAME_MAX];
  snprintf(localdir, NAME_MAX, "%s/%s", app_config->homedir_path, ".local");
  struct stat l;

  if (stat(localdir, &l) == -1) {

    fprintf(stderr, "[-] cannot stat() %s dir: %s\n",
            localdir,
            strerror(errno));

    return 0;

  }
  if (!S_ISDIR(l.st_mode)) {
    fprintf(stderr, "[-] error: %s is not a directory\n", localdir);
    return 0;
  }

  if (stat(app_config->application_dir_path, &l) == -1) {
    if (errno == ENOENT) {

      fprintf(stderr,
              "[WARN] %s does not exist, trying to create it\n",
              app_config->application_dir_path);

      if (mkdir(app_config->application_dir_path, 0755) == -1) {
        perror("[-] mkdir() failure");
        return 0;
      } else {

        printf("[+] application directory %s is ready\n",
               app_config->application_dir_path);

      }

    } else {
      perror("[-] stat() failure for app directory");
    }
  } else {
    printf("[+] app directory ready\n");
  }

  return 1;
}
