#include <sys/stat.h>

#include "config.main.h"
#include "data_api.h"
#include "dbinit.h"
#include "links_app.h"
#include "prepare.h"

int
main(int argc, char **argv)
{
  static const char *application_dir_ = APPLICATION_DIRECTORY;
  static const char *links_db_name_ = LINKS_DB_NAME;
  static struct lc_application_config appconfig_;

  appconfig_.homedir_path = getenv("HOME");

  if (appconfig_.homedir_path == NULL) {
    perror("[-] failed to locate home directory");
    exit(EXIT_FAILURE);
  }

  g_snprintf(appconfig_.application_dir_path,
             NAME_MAX,
             "%s/%s",
             appconfig_.homedir_path,
             application_dir_);

  prepare_application_directory(&appconfig_);

  g_snprintf(appconfig_.db_location,
             NAME_MAX,
             "%s/%s",
             appconfig_.application_dir_path,
             links_db_name_);

  fprintf(stderr, "[LOG] Should locate db at %s\n", appconfig_.db_location);

  if (argc > 1 && strcmp(argv[1], "--init") == 0) {
    sqlite3 *db = init_db(appconfig_.db_location, init_sql_string_default);
    if (db == NULL) {
      exit(EXIT_FAILURE);
    }
    sqlite3_close_v2(db);
    puts("Initialization complete\n");
    exit(EXIT_SUCCESS);
  }

  GtkApplication *app = gtk_application_new(LINKS_CATALOG_APPLICATION_ID,
					    G_APPLICATION_DEFAULT_FLAGS);

  struct lc_application_info inf;
  g_signal_connect(app, "activate", G_CALLBACK(do_app), &inf);
  int return_code = g_application_run(G_APPLICATION(app), argc, argv);
  if (inf.data) {
    fprintf(stderr, "Closing db connecion\n");
    data_close(inf.data);
  }
  return return_code;
}
