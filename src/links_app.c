
#include <sqlite3.h>

#include "data_api.h"
#include "links_app.h"

#define DEF_BUF_SIZE 2048
static const char href_format_str_[] = "<a href=\"%s\">%s</a>";

static void
ui_insert_callback(const struct lc_application_info *gui_info,
                   const char *markup_text);

static char *
escape_ampersand(const gchar *href_text) {
  size_t ampcount = 0;
  size_t href_len = strlen(href_text);

  for (size_t i = 0; i < href_len; ++ i) {
    if (href_text[i] == '&') {
      ++ ampcount;
    }
  }
  if (ampcount == 0) {
    return NULL;
  }
  size_t buflen = href_len + ampcount * 4 + 1;

  char *buf = (char *)malloc(buflen);

  size_t j = 0;
  for (size_t i = 0; i < href_len; ++ i) {
    if (href_text[i] == '&') {
      g_snprintf(buf + j, 6, "%s", "&amp;");
      j += 5;
    } else {
      buf[j] = href_text[i];
      ++ j;
    }
  }
  buf[j] = '\0';

  return buf;
}

static void
message_box(const char *title, const char* msg)
{
    fprintf(stderr, "%s\n", msg);
    GtkWidget* dialog = gtk_message_dialog_new(NULL, GTK_DIALOG_MODAL,
                                               GTK_MESSAGE_INFO,
                                               GTK_BUTTONS_OK,
                                               "%s",
                                               title);
    gtk_message_dialog_format_secondary_text(GTK_MESSAGE_DIALOG(dialog),
                                              "%s", msg);
    gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(GTK_WIDGET(dialog));
}

static void
insert_link_clicked_callback(struct lc_application_info *info) {
  const gchar *href = gtk_entry_get_text(info->user_link_entry);
  const gchar *remarktext = gtk_entry_get_text(info->user_remark_entry);
  GtkEntryBuffer *buf = NULL;
  const char *escaped;
  const char *remark_escaped;
  if (strlen(href) == 0 || strlen(remarktext) == 0) {
	  message_box("Insert Failed", "Cannot insert empty field");
	  gtk_label_set_text(info->status_label,
			"Unable to insert empty field(s)");
	  return;
  }
  if ((remark_escaped = escape_ampersand(remarktext)) == NULL)
    remark_escaped = remarktext;
  if ((escaped = escape_ampersand(href)) == NULL)
    escaped = href;
  if (strlen(remark_escaped) + strlen(escaped) >= DEF_BUF_SIZE) {
    message_box("Insert Failed", "Can't insert entry: text too long");
    gtk_label_set_text(info->status_label, "CANNOT INSERT: TEXT TOO LONG!");
    return;
  }
  if (data_append_row(info->data, escaped, remark_escaped) ==
      DATA_SUCCESS_FLAG) {
    char fulltext[DEF_BUF_SIZE];
    g_snprintf(fulltext, sizeof(fulltext), href_format_str_,
               escaped, remark_escaped);
    ui_insert_callback(info, fulltext);
  } else {
	  gtk_label_set_text(info->status_label, data_error_string(info->data));
  }
  if (escaped && escaped != href)
    free((void *)escaped);
  if (remark_escaped && remark_escaped != remarktext)
    free((void *)remark_escaped);
  if (info->user_link_entry) {
    buf = gtk_entry_get_buffer(info->user_link_entry);
    if (buf) {
      gtk_entry_buffer_set_text(buf, "", 0);
    }
  }
  if (info->user_remark_entry) {
    buf = gtk_entry_get_buffer(info->user_remark_entry);
    if (buf) {
      gtk_entry_buffer_set_text(buf, "", 0);
    }
  }
}

static void
quit_button_clicked(GtkWidget *b, GtkWindow *main_window) {
  (void)b;
  gtk_window_close(main_window);
}

static gboolean
escape_key_callback(GtkWidget *widget, GdkEventKey *event, gpointer user_data)
{
  if (event->keyval == GDK_KEY_Escape) {
    GtkWidget *main_window = GTK_WIDGET(widget);
    fprintf(stderr, "DEBUG: Escape pressed\n");
    gtk_window_close(GTK_WINDOW(main_window));
  }
  return FALSE;
  (void)user_data;
}

static void
filter_entry_changed_callback(GtkWidget *widget, gpointer data)
{
	gtk_list_box_invalidate_filter(GTK_LIST_BOX(data));
	(void)widget;
}

static gboolean
row_filter_callback(GtkListBoxRow *r, gpointer data)
{
	GtkWidget *l = GTK_WIDGET(gtk_bin_get_child(GTK_BIN(r)));
	if (l) {
		const struct lc_application_info *info =
			(const struct lc_application_info *)
			data;
		const char *text = gtk_label_get_text(GTK_LABEL(l));
		const char *label_uri =
			gtk_label_get_current_uri(GTK_LABEL(l));
		const char *needle =
			gtk_entry_get_text(info->filter_entry);
		if (text && label_uri && needle) {
			return strlen(needle) < 4 ||
					strstr(text, needle) != NULL ||
					strstr(label_uri, needle) != NULL;
		} else {
			return FALSE;
		}
	}
	return TRUE;
}

static int
build_ui(GtkWindow *app_window, struct lc_application_info *appinfo)
{
  GtkScrolledWindow *scroller =
      GTK_SCROLLED_WINDOW( gtk_scrolled_window_new(NULL, NULL) );
  g_signal_connect(app_window, "key-press-event",
                   G_CALLBACK(escape_key_callback),
                   app_window);

  gtk_widget_set_vexpand(GTK_WIDGET(scroller), TRUE);

  appinfo->links_list_box = GTK_LIST_BOX( gtk_list_box_new() );
  gtk_list_box_set_selection_mode(appinfo->links_list_box, GTK_SELECTION_NONE);
  gtk_list_box_set_filter_func(appinfo->links_list_box, row_filter_callback,
			       appinfo, NULL);

  GtkBox *widgets_box = GTK_BOX( gtk_box_new(GTK_ORIENTATION_VERTICAL, 2) );
  appinfo->status_label = GTK_LABEL( gtk_label_new("Application Ready") );
  gtk_label_set_line_wrap(appinfo->status_label, TRUE);
  gtk_widget_set_halign(GTK_WIDGET(appinfo->status_label), GTK_ALIGN_START);
  gtk_container_add(GTK_CONTAINER(widgets_box),
                    GTK_WIDGET(appinfo->status_label));
  GtkBox *insert_controls_box =
      GTK_BOX( gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 2) );

  GtkButton *insert_link_button =
      GTK_BUTTON( gtk_button_new_with_label("Add link") );

  g_signal_connect_swapped(insert_link_button,
                   "clicked",
                   G_CALLBACK(insert_link_clicked_callback),
                   appinfo);

  gtk_box_set_homogeneous(insert_controls_box, gtk_true());

  appinfo->user_link_entry = GTK_ENTRY( gtk_entry_new() );

  gtk_entry_set_placeholder_text(appinfo->user_link_entry,
                                 "Place your new link here");

  appinfo->user_remark_entry = GTK_ENTRY( gtk_entry_new() );
  gtk_entry_set_placeholder_text(appinfo->user_remark_entry,
                                 "Description of link");

  gtk_container_add(GTK_CONTAINER(insert_controls_box),
                    GTK_WIDGET(appinfo->user_link_entry));

  gtk_container_add(GTK_CONTAINER(insert_controls_box),
                    GTK_WIDGET(appinfo->user_remark_entry));

  gtk_container_add(GTK_CONTAINER(insert_controls_box),
                   GTK_WIDGET(insert_link_button));

  gtk_container_add(GTK_CONTAINER(scroller),
                    GTK_WIDGET(appinfo->links_list_box));

  appinfo->filter_entry = GTK_ENTRY(gtk_entry_new());
  gtk_entry_set_placeholder_text(appinfo->filter_entry, "Filter links");
  g_signal_connect(appinfo->filter_entry, "changed",
		   G_CALLBACK(filter_entry_changed_callback),
		   appinfo->links_list_box);
  gtk_container_add(GTK_CONTAINER(widgets_box),
		GTK_WIDGET(appinfo->filter_entry));

  gtk_container_add(GTK_CONTAINER(widgets_box),
                    GTK_WIDGET(insert_controls_box));

  gtk_container_add(GTK_CONTAINER(widgets_box), GTK_WIDGET(scroller));

  gtk_container_add(GTK_CONTAINER(app_window), GTK_WIDGET(widgets_box));

  GtkButton *quit_button =  GTK_BUTTON( gtk_button_new_with_label("Quit") );

  g_signal_connect(quit_button,
                   "clicked",
                   G_CALLBACK(quit_button_clicked),
                   app_window);

  gtk_container_add(GTK_CONTAINER(widgets_box), GTK_WIDGET(quit_button));
  return 0;
}

void
do_app(GtkApplication *app, struct lc_application_info *inf) {
  GtkWindow *main_window = GTK_WINDOW( gtk_application_window_new(app) );
  gtk_window_set_default_size(main_window, 400, 600);
  build_ui(main_window, inf);

  if (data_open(&inf->data) == DATA_ERROR_FLAG) {
    gtk_label_set_text(inf->status_label, "Failed to fetch data");
  } else {
    char *link;
    char *text;
    char fulltext[DEF_BUF_SIZE];
    while (data_next_record(inf->data, &link, &text) == DATA_SUCCESS_FLAG) {
      g_snprintf(fulltext, sizeof(fulltext), href_format_str_, link, text);
      ui_insert_callback(inf, fulltext);
    }
  }
  gtk_widget_show_all(GTK_WIDGET(main_window));
}

static gboolean
link_label_over_callback(GtkWidget *w, void *p,
                         const struct lc_application_info *info)
{
  if (GTK_IS_LABEL(w)) {
    GtkLabel *l = GTK_LABEL(w);
    const gchar *link_text = gtk_label_get_current_uri(l);
    if (link_text &&
        strcmp(gtk_label_get_text(info->status_label), link_text)) {
      gtk_label_set_text(info->status_label, link_text);
    }
  }
  return FALSE;
  (void)p;
}

void
ui_insert_callback(const struct lc_application_info *gui_info,
                   const char *markup_text)
{
  GtkWidget *link_label = gtk_label_new(NULL);
  gtk_label_set_markup(GTK_LABEL(link_label), markup_text);
  gtk_list_box_insert(gui_info->links_list_box, link_label, -1);
  g_signal_connect(link_label, "motion-notify-event",
                           G_CALLBACK(link_label_over_callback),
                           (void *)gui_info);
  gtk_widget_show(link_label);
}
