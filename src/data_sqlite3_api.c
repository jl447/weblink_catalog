#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sqlite3.h>

#include "data_api.h"

#define PRIV_SIZE 4096

struct data_connection {
  sqlite3 *db;
  sqlite3_stmt *sql_statement;
};

const char select_all[] = "select link,remark from links;";
const char insert_columns[] = "insert into links (link,remark) values (?, ?);";
const char local_path[] = ".local/links_catalog/links.db";

static size_t
strlcat(char *dst, const char *src, size_t siz);

enum data_result
data_append_row(void *data, const char *link, const char *remark)
{
  enum data_result res = DATA_ERROR_FLAG;
  struct data_connection *c = (struct data_connection *)data;
  if (sqlite3_prepare_v2(c->db, insert_columns, -1, &c->sql_statement, NULL)
      != SQLITE_OK) {
    fprintf(stderr, "%s\n", sqlite3_errmsg(c->db));
    return DATA_ERROR_FLAG;
  }
  printf("DEBUG: Inserting link \"%s\" with text \"%s\" for database object "
         "%p, with sql statement object %p\n",
         link, remark, c->db, c->sql_statement);
  sqlite3_bind_text(c->sql_statement, 1, link, -1, SQLITE_STATIC);
  sqlite3_bind_text(c->sql_statement, 2, remark, -1, SQLITE_STATIC);
  if (sqlite3_step(c->sql_statement) == SQLITE_DONE) {
    res = DATA_SUCCESS_FLAG;
  } else {
    fprintf(stderr, "%s\n", sqlite3_errmsg(c->db));
  }
  sqlite3_finalize(c->sql_statement);
  c->sql_statement = NULL;
  return res;
}

void
data_close(void *data)
{
  struct data_connection *fdb = (struct data_connection *)data;
  sqlite3_finalize(fdb->sql_statement);
  sqlite3_close_v2(fdb->db);
  fdb->db = NULL;
  fdb->sql_statement = NULL;
}

const char *
data_error_string(void *data_handle)
{
	struct data_connection *c = (struct data_connection *)data_handle;
	return (sqlite3_errmsg(c->db));
}

enum data_result
data_next_record(void *data, char **link_column, char **remark_column)
{
  struct data_connection *c = (struct data_connection *)data;
  int sel_res;
  enum data_result result = DATA_ERROR_FLAG;
  if (c->sql_statement == NULL) {
    sqlite3_prepare(c->db, select_all, sizeof(select_all),
                    &c->sql_statement, 0);
  }
  sel_res = sqlite3_step(c->sql_statement);
  if (sel_res == SQLITE_ROW) {
    *link_column = (char *)sqlite3_column_text(c->sql_statement, 0);
    *remark_column = (char *)sqlite3_column_text(c->sql_statement, 1);
    result = DATA_SUCCESS_FLAG;
  } else if (sel_res == SQLITE_DONE) {
    sqlite3_finalize(c->sql_statement);
    c->sql_statement = NULL;
    result = DATA_NO_MORE_DATA_FLAG;
  }
  return result;
}

enum data_result
data_open(void **data)
{
  struct data_connection *output_fd = malloc(sizeof(struct data_connection));
  enum data_result res = DATA_ERROR_FLAG;
  int sql_res;
  char links_db_path[PRIV_SIZE] = {0};
  output_fd->db = NULL;
  output_fd->sql_statement = NULL;
  strlcat(links_db_path, getenv("HOME"), PRIV_SIZE);
  links_db_path[strlen(links_db_path)] = '/';
  strlcat(links_db_path, local_path, PRIV_SIZE);
  sql_res = sqlite3_open_v2(links_db_path, &output_fd->db,
                            SQLITE_OPEN_READWRITE, 0);
  if (sql_res == SQLITE_OK) {
    res = DATA_SUCCESS_FLAG;
  } else {
    free(output_fd);
    output_fd = NULL;
  }
  *data = (void *)output_fd;
  return res;
}

size_t
strlcat(dst, src, siz)
  char *dst;
  const char *src;
  size_t siz;
{
  register char *d = dst;
  register const char *s = src;
  register size_t n = siz;
  size_t dlen;
  while (n-- != 0 && *d != '\0')
    d++;
  dlen = d - dst;
  n = siz - dlen;
  if (n == 0)
    return(dlen + strlen(s));
  while (*s != '\0') {
    if (n != 1) {
      *d++ = *s;
      n--;
    }
    s++;
  }
  *d = '\0';
  return(dlen + (s - src));
}
